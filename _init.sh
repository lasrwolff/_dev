#!/bin/bash

## export config
export PROJECT_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/../")"
export PROJECT_HASH="$(echo -n "$PROJECT_DIR" | md5)"
export PROJECT_HUMAN_ID="${PROJECT_HASH:0:8}""-""$(basename "$PROJECT_DIR")"   # unique and human readable identifier
export PROJECT_VOLUME_NAME="workdir_$PROJECT_HUMAN_ID"

export SSH_KEY_DIR=~/.ssh

export MACHINE_CPUS=4
export MACHINE_MEMORY=4096
export MACHINE_DISK_SIZE=20000

export SYNCIGNORE_FILE="$PROJECT_DIR/_dev/.syncignore"


## run additional initialization
echo "initialized project: $PROJECT_DIR"
#dev machine start
#dev ssh-agent start
#dev sync start