#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

IMAGE_TAG="devcli_${PROJECT_HASH:0:8}"
IMAGE_SOURCE="$SCRIPT_DIR/devcli"
CONTAINER_NAME="devcli_$PROJECT_HUMAN_ID"

function ensure_started {
  dev docker-helper ensure-build "$IMAGE_SOURCE" "$IMAGE_TAG"

  # ensure ssh-agent is started
  (dev ssh-agent start)

  # recreate existing container or start a new one
  if [ "$(docker ps -aq -f status="running" -f name="$CONTAINER_NAME")" ]; then
      echo "container \"$CONTAINER_NAME\" already running"
  elif [ "$(docker ps -aq -f status="exited" -f name="$CONTAINER_NAME")" ]; then
      echo "restarting old dev container \"$CONTAINER_NAME\""
      docker start "$CONTAINER_NAME"
  else
      echo "creating container \"$CONTAINER_NAME\""
      docker run -d \
          --volumes-from=ssh-agent -e SSH_AUTH_SOCK=/.ssh-agent/proxy-socket \
          -v "$PROJECT_VOLUME_NAME":/workspace \
          --name "$CONTAINER_NAME" \
          "$IMAGE_TAG"
  fi
}

command_name="$1"; shift
case "$command_name" in
build)
  dev docker-helper build "$IMAGE_SOURCE" "$IMAGE_TAG"
  ;;

clean)
  dev docker-helper container-remove "$CONTAINER_NAME"
  ;;

start)
  ensure_started
  ;;

stop)
  docker stop "$CONTAINER_NAME"
  ;;

login)
  ensure_started
  # cd into current project directory and start a login bash
  docker exec -it "$CONTAINER_NAME" bash -c "cd $(realpath --relative-to="$PROJECT_DIR" "$PWD"); bash -l"
  ;;

*)
  >&2 echo "ERROR: expected command. Usage: vm (build|clean|start|stop|login)"
esac
