#!/bin/bash

docker volume create portainer_data

if [ ! "$(docker ps -aq -f status="running" -f name="portainer")" ]; then
  echo "starting portainer"
  docker run -d \
    --rm \
    -p 9000:9000 \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    --name "portainer" \
    portainer/portainer
fi

# start portainer ui in webbrowser
python -mwebbrowser "http://${DOCKER_MACHINE_IP:-127.0.0.1}:9000"
