#!/bin/bash

#
# helper for common docker operations (mostly for conveinience)
#

command_name="$1"; shift
case "$command_name" in
build)
  # build a docker image from Dockerfile/context
  # params:
  # image_context_dir   # directory that holds the Dockerfile (and additional build context)
  # image_tag           # tag that should be set for the generated image

  function docker_build {
    local image_context_dir="$1"
    local image_tag="$2"
    echo "building image \"$image_tag\""
    docker build -t "$image_tag" "$image_context_dir"
  }
  docker_build "$@"
  ;;

ensure-build)
  # build a dockerfile only if the image is not available on the system
  # params: see docker_build

  function docker_ensure_build {
    local image_source="$1"
    local image_name="$2"
    if [ ! "$(docker image ls -aq "$image_name")" ]; then
      dev docker-helper build "$image_source" "$image_name"
    fi
  }
  docker_ensure_build "$@"
  ;;

container-remove)
  # remove a docker container if it exists
  # params:
  # container_name    # name of the container to be removed

  function docker_container_remove {
    local container_name="$1"

    if [ "$(docker ps -aq -f name="$container_name")" ]; then
      docker rm "$container_name"
    fi
  }
  docker_container_remove "$@"
  ;;

*)
  >&2 echo "command not found"
esac
