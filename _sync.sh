#!/bin/bash
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# syncs project dir to docker volume using rsync

IMAGE_TAG="rsyncd"
IMAGE_SOURCE="$SCRIPT_DIR/rsyncd"
CONTAINER_NAME="rsyncd_$PROJECT_HUMAN_ID"

function run_rsyncd {
  docker volume create "$PROJECT_VOLUME_NAME"
  # ensure rsync endpoint is running
  if [ ! "$(docker ps -aq -f status="running" -f name="$CONTAINER_NAME")" ]; then
    echo "creating container $CONTAINER_NAME"
    docker run -d \
      --rm \
      -v "$PROJECT_VOLUME_NAME":/syncdir \
      -p 873:873 \
      --name "$CONTAINER_NAME" \
      "$IMAGE_TAG"
  fi
  export PROJECT_VOLUME_NAME="$PROJECT_VOLUME_NAME"
}

function run_sync {
  rsync -rlptgoD --delete --chown=1000:1000 \
    --exclude-from "$SYNCIGNORE_FILE" \
    "$PROJECT_DIR/" ${DOCKER_MACHINE_IP:-127.0.0.1}::sync
}

case "$1" in
stop)
  docker stop "$CONTAINER_NAME"
  ;;

build)
  dev docker-helper build "$IMAGE_SOURCE" "$IMAGE_TAG"
  ;;

clean)
  run_rsyncd
  docker exec "$CONTAINER_NAME" sh -c "rm -rf /syncdir/..?* /syncdir/.[!.]* /syncdir/*"
  ;;

watch)
  dev docker-helper ensure-build "$IMAGE_SOURCE" "$IMAGE_TAG"
  run_rsyncd
  run_sync
  fswatch -0 -l 1 -o "$PROJECT_DIR" | xargs -0 -n1 -P1 -t "$SCRIPT_DIR/_sync.sh" sync
  ;;

*) # automatically start rsync endpoint and run sync
  dev docker-helper ensure-build "$IMAGE_SOURCE" "$IMAGE_TAG"
  run_rsyncd
  run_sync
esac
