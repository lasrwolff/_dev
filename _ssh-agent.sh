#!/bin/bash

# starts docksal/ssh-agent image and allows to add keys from local machine
#
# example: connect to ssh-agent from container and list active keys:
# docker run -it --rm --volumes-from=ssh-agent -e SSH_AUTH_SOCK=/.ssh-agent/proxy-socket docksal/ssh-agent ssh-add -l

IMAGE_NAME="docksal/ssh-agent"
CONTAINER_NAME="ssh-agent"

function run_agent {
  # ensure ssh-agent container is running
  if [ ! "$(docker ps -aq -f status="running" -f name="$CONTAINER_NAME")" ]; then
    echo "creating container $CONTAINER_NAME"
    docker run -d --rm \
      --name "$CONTAINER_NAME" \
      "$IMAGE_NAME"
  fi
}

command_name="$1"; shift
case "$command_name" in
start)
  run_agent
  ;;

stop)
  docker stop "$CONTAINER_NAME"
  ;;

add)
  run_agent

  KEY_SOURCE_DIR="$SSH_KEY_DIR"
  # copy ssh keys to docker machine if there is one running
  if [ "$DOCKER_MACHINE_NAME" ]; then
    echo "copying ssh keys from \"$SSH_KEY_DIR\" to machine \"$DOCKER_MACHINE_NAME\""
    KEY_SOURCE_DIR="/home/docker/.project_ssh"
    docker-machine scp --recursive --quiet "$SSH_KEY_DIR" docker@"$DOCKER_MACHINE_NAME":"$KEY_SOURCE_DIR"
  fi

  # add key to ssh-agent, default to id_rsa
  docker run -it --rm \
    --volumes-from="$CONTAINER_NAME" \
    -v "$KEY_SOURCE_DIR":/root/.ssh \
    "$IMAGE_NAME" \
    ssh-add "/root/.ssh/""${1:-id_rsa}"
  ;;

ls)
  run_agent
  docker run -it --rm \
    --volumes-from=ssh-agent \
    -e SSH_AUTH_SOCK=/.ssh-agent/proxy-socket \
    docksal/ssh-agent \
    ssh-add -l
  ;;

reset)
  run_agent
  docker run -it --rm \
    --volumes-from="$CONTAINER_NAME" \
    -v ~/.ssh:/root/.ssh \
    "$IMAGE_NAME" \
    ssh-add -D
  ;;

*)
  >&2 echo "ERROR: expected command. Usage: ssh-agent (start|stop|add|reset)"
esac
