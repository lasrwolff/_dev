# _dev
_dev gives you a structure to organize small scripts and commands that are used during 
development. It is based on bash scripting and tested on MacOS but running on linux should work as well.

It is especially meant as a helper for setups that use docker containers, because such setups tend to use
a lot of hard-to-remember and long commands.


# Install/Setup
Lets say you have your directories organized like this:
```
/projects/
/projects/my_project1
/projects/my_project2
/projects/my_project2/subdir
```
To use _dev, just clone this repository in a directory that is a common parent to all your projects.
E.g. to `/projects/_dev`

To use _dev in a project, (e.g. in `/projects/my_project2`), create a directory `_dev` and add an init
script (make sure it's executable) that sets the project configuration:
```
/projects/my_project2/_dev/_init.sh
```
`_init.sh` from this repo can be used as a template for init scripts.

## add to .bash_profile
For convenience you can automate activation and project initialization. Add to `~/.bash_profile` (assuming
you have _dev at path `~/projects/_dev`)
```
# setting up _dev
source "$HOME/projects/_dev/activate" 2> /dev/null
```


# Basic Usage
running ```source _dev/activate``` will add a function called `dev` to your current bash
environment. Whenever you use that command it will walk up the file system hierarchy looking for
a directory called `_dev` and for an appropriately named script file inside that directory.

for example: `dev foo` will try to find (in that order) and execute the first matching script:
```
./_dev/_foo.sh
../_dev/_foo.sh
../../_dev/_foo.sh
...
```

Also when activate is called, the script `_init.sh` will automatically be run, allowing you set
configuration etc.


# Commands (incomplete list)

### docker admin UI:
`dev portainer`
will start a ui for the current docker machine and open it in your default webbrowser. For initial setup you
might need to reload the login page that shows up to set a password. When you are asked to select an environment,
select "local"


### ssh-agent
`dev ssh-agent add`
allows to add ssh keys to an ssh-agent that is running as a conatiner and that can be used by other containers
