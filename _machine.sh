#!/bin/bash

MACHINE_NAME="$PROJECT_HUMAN_ID"

command_name="$1"; shift
case "$command_name" in
start)
  if [ "$(docker-machine status "$MACHINE_NAME" 2> /dev/null)" ]; then
    docker-machine start "$MACHINE_NAME"
  else
    docker-machine create \
      --driver "virtualbox" \
      --virtualbox-cpu-count "$MACHINE_CPUS" \
      --virtualbox-memory "$MACHINE_MEMORY" \
      --virtualbox-disk-size "$MACHINE_DISK_SIZE" \
      --virtualbox-no-share \
      "$MACHINE_NAME"
  fi
  eval $(docker-machine env "$MACHINE_NAME")
  export DOCKER_MACHINE_IP=$(docker-machine ip "$MACHINE_NAME")
  echo "DOCKER_MACHINE_IP=$DOCKER_MACHINE_IP"

  # start common services
  (dev ssh-agent start)
  ;;

stop)
  docker-machine stop "$MACHINE_NAME"
  # cleanup env (go back to local docker)
  unset DOCKER_TLS_VERIFY
  unset DOCKER_HOST
  unset DOCKER_CERT_PATH
  unset DOCKER_MACHINE_NAME
  unset DOCKER_MACHINE_IP
  ;;

*) # proxy all other commands to docker-machine
  docker-machine "$command_name" "$MACHINE_NAME" "$@"
esac
